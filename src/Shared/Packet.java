package Shared;

import java.util.List;

public class Packet 
{
    public enum Opcode 
    {
        RES,
        MOD,
        DEL,
        UPD,
        NEW,
        TICK, 
        PING,
    };

    private Opcode m_opcode;
    private int m_port;
    private List<String> m_data;
    
    public Packet(Opcode opcode, int port, List<String> data) 
    {
        m_opcode = opcode;
        m_port = port;
        m_data = data;
    }
    
    public Opcode getOpcode() 
    {
        return m_opcode;
    }

    public List<String> getData() 
    {
        return m_data;
    }
    

    public int getPort() 
    {
        return m_port;
    }

    public void setPort(int port) 
    {
        m_port = port;
    }
}
