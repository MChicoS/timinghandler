package Shared;

import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;

import Shared.Packet.Opcode;

public class Shared 
{
    // Variables compartidas de conexion
    private static final String MSG_START_STR = "START";            // String de inicio de mensaje
    private static final String MSG_END_STR = "END";                // String de fin de mensaje
    public static final int CLIENT_TO_SERVER_PORT = 1234;              // Puerto para la conexion C>S
    public static final int DEFAULT_CLIENT_LISTENER_PORT = 1235; // Puerto para la conexion S>C
    public static final String DEFAULT_SERVER_IP = "localhost";     // Host del servidor

    public static Socket clientSocket; //Socket del cliente
    public static ServerSocket serverSocket; // Socket para el servidor
    public static DataOutputStream outputData; //Flujo de datos de salida
    
    // Periodos de contadores
    public static final int TIMER_SERVER_PERIOD = 1;                 // Tiempo de periodo de reloj del servidor en ms: resolucion.
    public static final int TIMER_SERVER_UPDATE_UI_PERIOD = 60;     // Tiempo de periodo entre la actualizacion del valor de los timers en la UI
    public static final int TIMER_CLIENTS_UPDATE_PERIOD = 60;         // Tiempo de periodo de actualizacion temporal a los clientes
    public static final int TIMER_CLIENT_PING_PERIOD = 5000;         // Tiempo de periodo entre pings de los clients al servidor.
    public static final int TIMER_CLIENT_TIMEOUT_CHECKER = 1000;    // Tiempo de periodo entre cada verificacion del vencimiento del timeout de conexion.

    
    // Construye el paquete a partir del string recibido del servidor. (O)
    public static Packet buildPacketFromMessage(String message)
    {
        // Extraemos mensaje entre strings de START y END
        String targetMsg = message.substring(message.indexOf(MSG_START_STR) + new String(MSG_START_STR).length(), message.indexOf(MSG_END_STR));
        
        // Separamos el mensaje por comas
        String[] splittedMessage = targetMsg.split(",");
        
        // Introducimos cada campo de datos en el packet
        List<String> data = new ArrayList<String>();
        for (int i = 2; i <= splittedMessage.length - 1; i++)
            data.add(splittedMessage[i]);
        
        // Construimos el paquete con el opcode y los datos
        return new Packet(Opcode.valueOf(splittedMessage[0]), tryParse(splittedMessage[1]), data);
    }
    

    // Construye mensaje a partir del paquete para transmitir. (O)
    public static String buildMessageFromPacket(Packet packet) 
    {
        // Start
        String message = MSG_START_STR;
        // Opcode
        message += packet.getOpcode().toString();
        // Puerto del cliente
        message += "," + String.valueOf(packet.getPort());
        // Datos
        for (int i = 0; i < packet.getData().size(); i++)
            message += "," + packet.getData().get(i);
        // End
        message += MSG_END_STR;
        return message;
    }
    
    // Funcion que intenta parsear a int una entrada string, si es invalido devolverá 0. (O)
    public static int tryParse(String input)
    {
        int output = -1; // -1 es no parseable.
        try
        {
            output = Integer.parseInt(input);
        }
        catch(NumberFormatException e)
        {
        }
        return output;
    }
    
    // Devuelve un counter dada su ID (O)
    public static Counter getCounterById(List<Counter> counters, int id)
    {
        // Output null en principio
        Counter output = null;
        
        // Barremos para comprobar ID y client
        for (int i = 0; i < counters.size(); i++)
        {
            Counter counter = counters.get(i);
            
            // Si el counter tiene el ID y cliente deseados, es el que buscamos.
            if (counter.getId() == id)
                output = counter;
        }
        return output;
    }
    
    // Devuelve un counter dada su ID y cliente. (O)
    public static Counter getCounterByIdAndClient(List<Counter> counters, int id, Client client)
    {
        // Output null en principio
        Counter output = null;
        
        // Barremos para comprobar ID y client
        for (int i = 0; i < counters.size(); i++)
        {
            Counter counter = counters.get(i);
            String counterIpAdress = counter.getClient().getIpAdress();
            String counterPort = String.valueOf(counter.getClient().getListenPort());
            
            // Si el counter tiene el ID y cliente deseados, es el que buscamos.
            if (counter.getId() == id)
                if (new String(counterIpAdress).equals(client.getIpAdress())) // Coinciden IPs
                    if (new String(counterPort).equals(String.valueOf(client.getListenPort()))) // Coinciden puertos
                output = counter;
        }
        return output;
    }
    
    // Devuelve la lista de counters para el cliente deseado. (O)
    public static List<Counter> getCountersByClient(List<Counter> counters, Client client)
    {
        // Output null en principio
        List<Counter> output = new ArrayList<Counter>();
        
        // Barremos para comprobar Client
        for (int i = 0; i < counters.size(); i++)
        {
            Counter counter = counters.get(i);
            String counterIpAdress = counter.getClient().getIpAdress();
            String counterPort = String.valueOf(counter.getClient().getListenPort());
            
            // Si el counter tiene el cliente deseado, add a la lista de counters de output
            if (new String(counterIpAdress).equals(client.getIpAdress())) // Coinciden IPs
                    if (new String(counterPort).equals(String.valueOf(client.getListenPort()))) // Coinciden puertos
                            output.add(counter);
        }
        return output;
    }
    
    // Devuelve la ID del contador con ID mas alta de la lista de contadores, empleada para asignar nuevas ids (O)
    public static int getHiggerCounterId(List<Counter> counters)
    {
        // Output -1 en principio
        int output = -1;
        
        // Barremos y asignamos output a la ID mas alta que encontremos
        for (int i = 0; i < counters.size(); i++)
        {
            Counter counter = counters.get(i);
            int id = counter.getId();
            if (id > output)
                output = id;
        }
        return output;
    }


    // Devuelve una lista de todos los clientes dada una lista de contadores. (O)
    public static List<Client> getAllServerClients(List<Counter> counters) 
    {
        // Output null en principio
        List<Client> output = new ArrayList<Client>();

        // Barremos todos los contadores
        for (int i = 0; i < counters.size(); i++)
        {
            Counter counter = counters.get(i);
            
            // Ip del contador actual
            Client client = counter.getClient();
            
            boolean isInList = false;
            // Comprobamos que no estaba ya
            for (int j = 0; j < output.size(); j++)
            {
                Client previousClient = output.get(j);
                String counterIpAdress = previousClient.getIpAdress();
                String counterPort = String.valueOf(previousClient.getListenPort());
                
                if (new String(counterIpAdress).equals(client.getIpAdress())) // Coinciden IPs
                    if (new String(counterPort).equals(String.valueOf(client.getListenPort()))) // Coinciden puertos
                        isInList = true;
            }

            
            // No hemos encontrado la IP, por lo que no estaba, add
            if (!isInList)
                output.add(client);
        }
        return output;
    }


    // Devuelve la longitud del string mas largo del log de eventos
    public static int getLongestStringOfLogList(DefaultListModel<String> eventLogList) 
    {
        // Output 0 en principio
        int output = 0;
        
        // Barremos y asignamos output a la longitud mas alta que encontremos
        for (int i = 0; i < eventLogList.size(); i++)
        {
            String row = eventLogList.get(i);
            int length = row.length();
            if (length > output)
                output = length;
        }
        return output;
    }


    // Metodo join
    public static String stringJoin(String string, List<String> data) 
    {
        String output = "";
        for (int i = 0; i < data.size(); i++)
        {
            // Ultimo elemento no string separador
            if (i == data.size() - 1)
                output += data.get(i);
            else
                output += data.get(i) + string;
        }
            
        return output;
    }
}
