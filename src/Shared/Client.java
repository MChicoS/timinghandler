package Shared;

public class Client 
{
    private String m_ipAdress;
    private int m_listenPort;
    
    public Client(String ipAdress, int listenPort) 
    {
        m_ipAdress = ipAdress;
        m_listenPort = listenPort;
    }
    
    public String getIpAdress() 
    {
        return m_ipAdress;
    }
    
    public void setIpAdress(String ipAdress) 
    {
        m_ipAdress = ipAdress;
    }
    public int getListenPort() 
    {
        return m_listenPort;
    }
    
    public void setListenPort(int listenPort) 
    {
        this.m_listenPort = listenPort;
    }
    
}
