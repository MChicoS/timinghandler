package Shared;

public class Counter 
{
    private int m_id; 
    private int m_startingTime;
    private int m_remainingTime;
    private Client m_client;
    private String m_tickString;

    
    public Counter(int id, int startValue, int currentValue, Client client, String tickString) 
    {
        m_id = id;
        m_startingTime = startValue;
        m_remainingTime = currentValue;
        m_client = client;
        m_tickString = tickString;
    }

    public int getId() 
    {
        return m_id;
    }

    public void setId(int id) 
    {
        m_id = id;
    }

    public int getStartingTime() 
    {
        return m_startingTime;
    }

    public void setStartingTime(int startValue) 
    {
        m_startingTime = startValue;
    }

    public int getRemainingTime() 
    {
        return m_remainingTime;
    }

    public void setRemainingTime(int currentValue) 
    {
        m_remainingTime = currentValue;
    }

    public Client getClient() 
    {
        return m_client;
    }
    
    public void setClient(Client client) 
    {
        m_client = client;
    }
    
    public String getTickString() 
    {
        return m_tickString;
    }

    public void setTickString(String tickString) 
    {
        m_tickString = tickString;
    }
}
