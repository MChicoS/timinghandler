package Client;

import java.util.List;
import java.util.Scanner;
import Client.ClientMain.clientState;
import Shared.Counter;
import Shared.Shared;

public class ClientConsoleInputListener extends Thread
{
    public enum Commands 
    {
        help,     // Muestra sus comandos y sintaxis:                         help
        show,     // Muestra todos los contadores del cliente en pantalla:    show
        add,      // Add nuevo contador con el valor dado (solo cliente):        add <VALUE> <TICKSTRING>
        res,    // Reinicia el contador con la ID dada:                        res <ID>
        mod,     // Modifica el contador con la ID dada:                        mod <ID> <VALUE>
        del,     // Borra el contador con la ID dada                            del <ID>
        ping,   // Envia un ping al server                                  ping
    }

    private ClientMain m_mainWindow;
    Scanner m_scanIn;
    
    // Ventana que ha ejecutado el thread
    public ClientConsoleInputListener(ClientMain window) 
    {
        m_mainWindow = window;
    }

    public void run()
    {
        m_scanIn = new Scanner(System.in);
        System.out.println("Timing Handler v1.2 (CLIENT)");
        setBindPort();
        setServerIp();
        System.out.println("List of available comamands:");
        for (Commands com : Commands.values()) 
            System.out.println(getCommandHelp(com));
        // Lectura permanente de comandos llegados a este punto
        while (true)
        {
            // Si en cualquier momento el cliente pasa a estar desconectado, forzamos introduccion de nueva IP
            if (m_mainWindow.m_clientState == clientState.BOUND_UNCONNECTED)
                setServerIp();
            else // Caso contrario, leemos comandos.
                readConsoleCommands();
        }
            
    }
    
    // Establecemos puerto al arrancar
    private void setBindPort() 
    {
        // Primera fase, cliente sin conectar ni bindear
        while (true)
        {
            System.out.println("Insert Listener port: ");
            
            String readString = m_scanIn.nextLine();
            
            // Leemos puerto de la casilla
            int port = Shared.tryParse(readString);
            
            // Si el puerto es 0 o no parseable, invalido.
            if (port == -1 || port == 0)
            {
                System.out.println("Port id must be a valid number.");
                continue;
            }
            else if (port == Shared.CLIENT_TO_SERVER_PORT) // No puede ocupar el mismo puerto que el servidor
            {
                System.out.println("Cannot be in the same port that the transmission socket.");
                continue;
            }
            
            // No lo bindea correctamente, ya hay otro cliente asignado a ese puerto
            if (!m_mainWindow.tryBindListenerPort(port))
            {
                System.out.println("The port you want to bind is already bound to another client.");
                continue;
            }
            else // Si es correcto, reportamos exito y abandonamos el loop
            {
                System.out.println("Client listening port bound at: " + port);
                break;
            }
        }
    }

    // Establecemos IP al arrancar
    private void setServerIp() 
    {
        System.out.println("Insert Server IP: ");
        // Segunda fase, cliente bindeado pero no conectado
        while (m_mainWindow.m_clientState == clientState.BOUND_UNCONNECTED)
        {
            String readString = m_scanIn.nextLine();

            // Si ya no esta en este estado (por un ping anterior) abortar.
            if (m_mainWindow.m_clientState != clientState.BOUND_UNCONNECTED)
                break;
            
            // No hay mensaje
            if (readString.equals(""))
                continue;
            
            // Intentamos conectar
            m_mainWindow.tryConnectServer(readString);
        }
    }

    // Modo lectura de comandos si el cliente ya esta online
    private void readConsoleCommands()
    {
        System.out.println("Enter command: ");
        String commandString = m_scanIn.nextLine();
        parseCommand(commandString);
    }

    private void parseCommand(String commandString) 
    {
        String[] commandArray = commandString.split(" ");
        
        // Intentamos parsear comando
        try
        {
            Commands command = Commands.valueOf(commandArray[0]);
            switch (command)
            {
                case help: // No params
                    System.out.println("List of available comamands:");
                    for (Commands com : Commands.values()) 
                        System.out.println(getCommandHelp(com));
                    break;
                case show: // No params
                    System.out.println("Showing all counters:");
                    // Leemos los contadores y printeamos
                    List<Counter> counters = m_mainWindow.getAllCounters();
                    for (int i = 0; i < counters.size(); i++)
                        System.out.println("Counter " + counters.get(i).getId() + " value " + counters.get(i).getStartingTime() + "ms/" + counters.get(i).getRemainingTime() + "ms and tick string: " + counters.get(i).getTickString());
                    break;
                case add: // value, tickstring
                    if (commandArray.length != 3)
                    {
                        System.out.println("Error parsing command '" + command + "' sintax: "  + getCommandHelp(command));
                        break;
                    }    
                    if (m_mainWindow.addCounter(Shared.tryParse(commandArray[1]), commandArray[2]))
                        System.out.println("NEW COUNTER: New counter with value " + commandArray[1] + "ms and tick string '" + commandArray[2] + "' has started.");
                    else
                        System.out.println("Error parsing command '" + command + "' sintax: "  + getCommandHelp(command));
                    break;
                case res: // id
                    if (m_mainWindow.resetCounter(Shared.tryParse(commandArray[1])))
                        System.out.println(("RESET: Counter " + commandArray[1] + " has been reset to " + Shared.getCounterById(m_mainWindow.getAllCounters(), Shared.tryParse(commandArray[1])).getStartingTime() + "ms."));
                    else
                        System.out.println("Error parsing command '" + command + "' sintax: "  + getCommandHelp(command));
                    break;
                case mod: // id, value
                    if (commandArray.length != 3)
                    {
                        System.out.println("Error parsing command '" + command + "' sintax: "  + getCommandHelp(command));
                        break;
                    }
                    if (m_mainWindow.modifyCounter(Shared.tryParse(commandArray[1]), Shared.tryParse(commandArray[2])))
                        System.out.println("MODIFY: Counter " + commandArray[1] + " value has been set to " + commandArray[2] + "ms.");
                    else
                        System.out.println("Error parsing command '" + command + "' sintax: "  + getCommandHelp(command));
                    break;
                case del: // id
                    if (m_mainWindow.deleteCounter(Shared.tryParse(commandArray[1])))
                        System.out.println("DELETE: Counter " + commandArray[1] + " has been deleted.");
                    else
                        System.out.println("Error parsing command '" + command + "' sintax: "  + getCommandHelp(command));
                    break;
                case ping: // No param
                    m_mainWindow.sendPingToServer();
                    break;
            }
        }
        catch (Exception e) // Si estamos aqui es que el comando no se ha reconocido
        {
            System.out.println("Unknown command. Tipe 'help' to view avaliable commands.");
        }
    }

    private String getCommandHelp(Commands command)
    {
        String output = "";
        switch (command)
        {
            case help:
                output = "'help' shows all avaliable commands.";
                break;
            case show:
                output = "'show' print all counters.";
                break;
            case add:
                output = "'add <value> <tickstring>' Adds new counter with the defined value, tickstring is the string that will be shown when the counter expires.";
                break;
            case res:
                output = "'res <id>' Resets the counter with the defined id.";
                break;
            case mod:
                output = "'mod <id> <value>' Sets the value of the counter with the defined id to the defined value.";
                break;
            case del:
                output = "'del <id>' Deletes the counter with the defined id.";
                break;
            case ping:
                output = "'ping' Sends a ping to the client.";
                break;
            default:
                output = "Unknown command. Tip 'help' to view avaliable commands.";
        }
        return output;
    }
}
