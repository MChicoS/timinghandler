package Client;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GraphicsEnvironment;

import javax.swing.JFrame;
import net.miginfocom.swing.MigLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.DefaultListModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import Shared.Client;
import Shared.Counter;
import Shared.Packet;
import Shared.Shared;

import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import java.awt.Dimension;
import Client.ClientListenThread;
import javax.swing.ImageIcon;

public class ClientMain
{
    /*
    // Sirve para testear el modo headless (sin interfaz) 
    static 
    { 
        System.setProperty("java.awt.headless", "true");
    }
    */
    
    // UI
    JPanel m_activeCounters;
    JScrollPane m_countersScrollPanel;
    DefaultListModel<String> m_eventLogList;
    JScrollPane m_eventLogScroll;
    JList<String> m_eventLog;
    JLabel m_serverStateLabel;
    JTextField m_serverIpTextBox;
    Timer m_pingTimer;
    TimerTask m_sendPing;
    JButton m_addCounterButton;
    JButton m_serverConnectButton;
    JLabel m_portStateLabel;
    JTextField m_clientListenPortTextBox;
    JTextField m_tickStringTextBox;
    JButton m_bindPortButton;
    
    // Instancias
    ClientListenThread m_listenThread;
    
    // Variables globales
    private List<Counter> m_counters = new ArrayList<Counter>();
    private int m_connectionTimeOut;
    clientState m_clientState;
    int m_clientListenerPort;
    public String m_serverIp;
    
    // Estado de conexion del cliente
    public enum clientState
    {
        UNBOUND_UNCONNECTED,
        BOUND_UNCONNECTED,
        BOUND_CONNECTED,
    };    

    // Metodo main, iniciamos el cliente (O)
    public static void main(String[] args) 
    {
        EventQueue.invokeLater(new Runnable() 
        {
            public void run() 
            {
                try 
                {
                    // Instanciamos la clase
                    ClientMain window = new ClientMain();
                    
                    // Inicializamos thread de consola
                    ClientConsoleInputListener consoleListenThread = new ClientConsoleInputListener(window);
                    consoleListenThread.start();
                } 
                catch (Exception e) 
                {
                    e.printStackTrace();
                }
            }
        });
    }
    
    // Constructor, inicializamos toda la UI (O)
    public ClientMain() 
    {
        // Inicializamos interfaz si existe
        if (!GraphicsEnvironment.isHeadless())
            initializeUi();
        
        // Temporizador de pings al server
        m_pingTimer = new Timer();
        // Transmitimos un ping al servidor para comprobar que esta online
        m_sendPing = new TimerTask()
        {
            public void run() 
            {
                // Enviamos ping al servidor
                sendPingToServer();
            }
        };
        
        // Temporizador de connection timeout
        Timer timeoutTimer = new Timer();
        // Transmitimos un ping al servidor para comprobar que esta online
        TimerTask checkConnectionTimeOut = new TimerTask()
        {
            public void run() 
            {
                // Si el timeout vence, consideramos que la conexion se ha perdido
                if (m_connectionTimeOut <= 0)
                {
                    // Conexion perdida
                    setServerState(false);
                    
                    // Reiniciamos el contador
                    resetTimeoutTimer();
                }
                else // Decrementamos
                    m_connectionTimeOut -= Shared.TIMER_CLIENT_TIMEOUT_CHECKER;
            }
        };
        timeoutTimer.scheduleAtFixedRate(checkConnectionTimeOut, 0, Shared.TIMER_CLIENT_TIMEOUT_CHECKER);
        
        // Estado inicial desconectado y desbindeado
        updateClientState(clientState.UNBOUND_UNCONNECTED);
    }
    
    
    /// ##############
    /// ###INTERFAZ###
    /// ##############

    // Metodo que inicializa todas las variables de la interfaz (O)
    public void initializeUi()
    {
        // Jframe principal
        JFrame mainWindowJFrame = new JFrame("Timing Handler v1.2 (CLIENT)");
        mainWindowJFrame.setResizable(false);
        mainWindowJFrame.setBounds(100, 100, 768, 423);
        mainWindowJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainWindowJFrame.getContentPane().setLayout(new MigLayout("", "[91px][8px][73px][10px][65px][55px][10px][65px][10px][83px][23px][242px]", "[14px][26px][3px][23px][6px][14px][7px][257px][24px]"));
        mainWindowJFrame.setVisible(true);
        
        // label de active counters
        JLabel activeCountersLabel = new JLabel("Active counters");
        activeCountersLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
        mainWindowJFrame.getContentPane().add(activeCountersLabel, "cell 0 0,alignx left,aligny top");
        
        // Casilla de Client Port
        m_clientListenPortTextBox = new JTextField();
        m_clientListenPortTextBox.setText(String.valueOf(Shared.DEFAULT_CLIENT_LISTENER_PORT));
        m_clientListenPortTextBox.setFont(new Font("Tahoma", Font.PLAIN, 11));
        m_clientListenPortTextBox.setColumns(10);
        m_clientListenPortTextBox.setEnabled(true);
        mainWindowJFrame.getContentPane().add(m_clientListenPortTextBox, "cell 7 1 2 1,growx,aligny center");
        
        // Boton para asignar el puerto
        m_bindPortButton = new JButton("Bind");
        m_bindPortButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) 
            {
                // Leemos puerto de la casilla
                int port = Shared.tryParse(m_clientListenPortTextBox.getText());
                
                // Si el puerto es 0 o no parseable, invalido.
                if (port == -1 || port == 0)
                {
                    JOptionPane.showMessageDialog(null, "Insert a valid number.");
                    return;
                }
                else if (port == Shared.CLIENT_TO_SERVER_PORT) // No puede ocupar el mismo puerto que el servidor
                {
                    JOptionPane.showMessageDialog(null, "Cannot be in the same port that the transmission socket.");
                    return;
                }
                
                tryBindListenerPort(port);
            }
        });
        mainWindowJFrame.getContentPane().add(m_bindPortButton, "cell 9 1,growx,aligny center");
        
        // Label de log de timers
        JLabel logDeTimersLabel = new JLabel("Event Log");
        logDeTimersLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
        mainWindowJFrame.getContentPane().add(logDeTimersLabel, "cell 5 5 1 2,alignx left,aligny bottom");
        
        // Label de tick string
        JLabel tickStringLabel = new JLabel("Tick message:");
        mainWindowJFrame.getContentPane().add(tickStringLabel, "cell 2 8 2 1,alignx right,aligny center");
        
        // Panel de scroll de los contadores
        m_countersScrollPanel = new JScrollPane();
        m_countersScrollPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        mainWindowJFrame.getContentPane().add(m_countersScrollPanel, "cell 0 1 5 7,grow");
        
        // Panel de contadores
        m_activeCounters = new JPanel();
        m_activeCounters.setLayout(null);
        m_activeCounters.setPreferredSize(new Dimension(0, 0));
        
        // Log de eventos, scroll y tabla
        m_eventLogList = new DefaultListModel<String>();
        m_eventLogScroll = new JScrollPane();
        mainWindowJFrame.getContentPane().add(m_eventLogScroll, "cell 5 7 7 1,grow");
        m_eventLog = new JList<String>(m_eventLogList);
        m_eventLogScroll.setViewportView(m_eventLog);
        m_eventLog.setFont(new Font("Tahoma", Font.PLAIN, 11));
        
        // Boton clear log
        JButton clearLogButton = new JButton("Clear Log");
        mainWindowJFrame.getContentPane().add(clearLogButton, "cell 5 8 3 1,grow");
        clearLogButton.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
                clearLog();
            }
        });
        
        // Etiqueta de estado del servidor
        m_serverStateLabel = new JLabel("");
        m_serverStateLabel.setBorder(null);
        m_serverStateLabel.setIcon(new ImageIcon(ClientMain.class.getResource("/Shared/sOffline.jpg")));
        mainWindowJFrame.getContentPane().add(m_serverStateLabel, "cell 10 3 2 1,growx,aligny center");
        
        // Etiqueta de estado del puerto
        m_portStateLabel = new JLabel("");
        m_portStateLabel.setBorder(null);
        m_portStateLabel.setIcon(new ImageIcon(ClientMain.class.getResource("/Shared/pBusy.jpg")));
        mainWindowJFrame.getContentPane().add(m_portStateLabel, "cell 10 1 2 1,growx,aligny center");
        
        // Boton nuevo contador
        m_addCounterButton = new JButton("Add counter");
        m_addCounterButton.setPreferredSize(new Dimension(99, 23));
        mainWindowJFrame.getContentPane().add(m_addCounterButton, "cell 0 8,growx,aligny top");
        m_addCounterButton.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
                // Sacamos ventana para introducir valor del nuevo counter
                String result = JOptionPane.showInputDialog("Enter counter value in ms:");
                
                // Si no pulsamos cancelar
                if (result != null)
                {
                    // Comprobamos que el valor es un integer
                    int value = Shared.tryParse(result);
                    if (!addCounter(value, m_tickStringTextBox.getText()))
                        JOptionPane.showMessageDialog(null, "Insert a valid number.");
                }
            }
        });
        
        // Label de Client Port
        JLabel clientPortLabel = new JLabel("Client Port:");
        mainWindowJFrame.getContentPane().add(clientPortLabel, "cell 5 1 2 1,alignx right,aligny center");
        
        // Etiqueta server IP
        JLabel serverLabel = new JLabel("Server IP:");
        mainWindowJFrame.getContentPane().add(serverLabel, "cell 5 3 2 1,alignx right,aligny center");
        
        // TextBox para direccion IP destino
        m_serverIpTextBox = new JTextField();
        m_serverIpTextBox.setFont(new Font("Tahoma", Font.PLAIN, 10));
        m_serverIpTextBox.setColumns(10);
        m_serverIpTextBox.setText(Shared.DEFAULT_SERVER_IP);
        mainWindowJFrame.getContentPane().add(m_serverIpTextBox, "cell 7 3 2 1,growx,aligny center");
        
        // Casilla de Tick String
        m_tickStringTextBox = new JTextField();
        m_tickStringTextBox.setText("tick");
        m_tickStringTextBox.setFont(new Font("Tahoma", Font.PLAIN, 11));
        m_tickStringTextBox.setColumns(10);
        mainWindowJFrame.getContentPane().add(m_tickStringTextBox, "cell 4 8,growx,aligny center");
        
        // Boton de conexion del servidor
        m_serverConnectButton = new JButton("Connect");
        m_serverConnectButton.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
                tryConnectServer(m_serverIpTextBox.getText());
            }
        });
        mainWindowJFrame.getContentPane().add(m_serverConnectButton, "cell 9 3,growx,aligny center");
    }
    
    // Imprimimos nuevo evento en el log de eventos con la fecha de este instante (O)
    public void printLogEvent(final String text)
    {
        // Abortamos si no hay interfaz
        if (GraphicsEnvironment.isHeadless())
            return;
        
        // Aseguramos que el update se encuentre en un thread independiente
        SwingUtilities.invokeLater(new Runnable() 
        {
            public void run() 
            {
                // Instante actual, en milisegundos.
                Format format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:SSS");
                String dateString = format.format(Calendar.getInstance().getTime());
                
                // add elemento.
                m_eventLogList.addElement(dateString + " " + text);
                
                // Longitud horizontal maxima para actualizar scroll
                int longestLength = Shared.getLongestStringOfLogList(m_eventLogList); 
                float pixelsPerChar = 5.5f; // Pixeles por caracter, usado para calcular la longitud visual de los strings.
                int finalSize = (int)(longestLength * pixelsPerChar);

                // Establecemos nueva longitud del panel que contiene los contadores.
                m_eventLog.setPreferredSize(new Dimension(finalSize, m_eventLogList.size() * 16));
            
                // Establecemos scroll del panel scrollable que lo contiene
                m_eventLogScroll.setViewportView(m_eventLog);
            }
        });
    }
    
    // Limpiamos todo el log (O)
    public void clearLog()
    {
        // Limpiamos log
        m_eventLogList.clear();
        
        // Restablecemos scroll
        m_eventLog.setPreferredSize(new Dimension(0, 0));
        m_eventLogScroll.setViewportView(m_eventLog);
    }
    
    // Actualiza el display de counters con sus valores actuales. (O)
    private void updateCountersDisplay()
    {
        // Abortamos si no hay interfaz
        if (GraphicsEnvironment.isHeadless())
            return;

        // Aseguramos que el update se encuentre en un thread independiente
        SwingUtilities.invokeLater(new Runnable() 
        {
            public void run() 
            {
                // Limpiamos display
                m_activeCounters.removeAll();
                m_activeCounters.setPreferredSize(new Dimension(0, 0));
                
                // Cargamos todos los contadores
                for (int i = 0; i < m_counters.size(); i++)
                {
                    if (m_counters == null)
                        continue;
                    Counter counter = m_counters.get(i);
                    // Cubrimos excepciones
                    if (counter == null)
                        continue;
                    
                    addNewTimerPanel(counter.getId(), counter.getStartingTime(), counter.getRemainingTime(), i);
                }
                
                // Repintamos la UI
                m_activeCounters.repaint();
            }
        });
    }

    // Anade un nuevo contador y sus controles asociados (O)
    public void addNewTimerPanel(final int counterId, int startingMs, int remainingMs, int rowNumber)
    {
        // Las posiciones son incrementales
        int heightGain = 60; // Separacion con respecto a la fila previa
        int pixelsPerChar = 6; // Pixeles por caracter, usado para calcular la longitud visual de los strings.
        int offSetCorrection = 4; // Correccion de posicion para valores altos.
        
        // Label: nombre del contador.
        JLabel counterNameLabel = new JLabel("Counter " + counterId);
        counterNameLabel.setBounds(0, heightGain * rowNumber, 90, 20);
        m_activeCounters.add(counterNameLabel);
        // Label: tiempo inicial del contador
        JLabel counterStartingTimeLabel = new JLabel(startingMs + "ms / ");
        counterStartingTimeLabel.setBounds(0, (heightGain * rowNumber) + 15, 120, 20);
        m_activeCounters.add(counterStartingTimeLabel);
        // Label: tiempo restante del contador.
        JLabel counterRemainingTimeLabel = new JLabel(remainingMs + "ms");
        counterRemainingTimeLabel.setBounds(counterStartingTimeLabel.getText().length() * pixelsPerChar + offSetCorrection , (heightGain * rowNumber) + 15, 120, 20);
        m_activeCounters.add(counterRemainingTimeLabel);

        // Botones
        JButton resetButton = new JButton("Reset");
        resetButton.setBounds(0, (heightGain * rowNumber) + 35, 70, 20);
        m_activeCounters.add(resetButton);
        JButton modifyButton = new JButton("Modify");
        modifyButton.setBounds(70, (heightGain * rowNumber) + 35, 80, 20);
        m_activeCounters.add(modifyButton);
        JButton deleteButton = new JButton("Delete");
        deleteButton.setBounds(150, (heightGain * rowNumber) + 35, 70, 20);
        m_activeCounters.add(deleteButton);

        // Acciones de botones
        resetButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) 
            {
                resetCounter(counterId);
            }
        });
        modifyButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) 
            {
                String result = JOptionPane.showInputDialog("Enter the new value in ms:");
    
                // Si no pulsamos cancelar
                if (result != null)
                {
                    int value = Shared.tryParse(result);
                    if (!modifyCounter(counterId, value))
                        JOptionPane.showMessageDialog(null, "Insert a valid number.");
                }
            }
        });
        deleteButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) 
            {
                deleteCounter(counterId);
            }
        });

        // Establecemos nueva dimension del scroll
        // dimension actual
        Dimension dim = m_activeCounters.getPreferredSize();
        // Establecemos nueva longitud del panel que contiene los contadores.
        m_activeCounters.setPreferredSize(new Dimension(160, dim.height + heightGain));
        // Establecemos scroll del panel scrollable que lo contiene
        m_countersScrollPanel.setViewportView(m_activeCounters);
    }
    
    // Metodo que se llama al pulsar el boton Connect (O)
    protected void tryConnectServer(String ip) 
    {
        // Establecemos IP
        m_serverIp = ip;
        
        // Mandamos un ping al servidor a ver si responde y lo consideramos online
        sendPingToServer();
    }
    
    // Metodo que se llama al pulsar el boton bind, devuelve false si el puerto ya esta bindeado a otro cliente (O)
    boolean tryBindListenerPort(int port)
    {
        // Intentamos asignar
        try
        {
            // Intentamos asignar puerto
            Shared.serverSocket = new ServerSocket(port);
            Shared.clientSocket = new Socket();

            // Inicializamos thread que escucha al servidor
            m_listenThread = new ClientListenThread(this);
            m_listenThread.start();
            
            // Estado
            printLogEvent("Client listening port bound at: " + port);
            
            // Establecemos el cliente como bound pero sin conectar
            updateClientState(clientState.BOUND_UNCONNECTED);
            
            // Actualizar interfaz si existe
            if (!GraphicsEnvironment.isHeadless())
                m_portStateLabel.setIcon(new ImageIcon(ClientMain.class.getResource("/Shared/pBound.jpg")));
            
            // Establecemos variable global de puerto
            m_clientListenerPort = port;
            
            return true;
        }
        catch (Exception e)
        {
            // Abortamos si hay interfaz grafica
            if (!GraphicsEnvironment.isHeadless())
                JOptionPane.showMessageDialog(null, "The port you want to bind is already bound to another client.");
            
            return false;
        }
    }
    
    
    /// ####################
    /// ###COMUNICACIONES###
    /// ####################

    // TRANSMISION CLIENTE (O)
    private void sendPacket(Packet packet)
    {
        // Construimos el mensaje
        String message = Shared.buildMessageFromPacket(packet);

        // Enviamos paquete
        try 
        {
            // Creamos socket
            Shared.clientSocket = new Socket(m_serverIp, Shared.CLIENT_TO_SERVER_PORT);
            
            // Salida de datos
            Shared.outputData = new DataOutputStream(Shared.clientSocket.getOutputStream());
            Shared.outputData.writeUTF(message + "\n");                
            
            //Fin de la conexion
            Shared.clientSocket.close();
            
            // Paquete enviado correctamente
            setServerState(true);
        } 
        catch (IOException e) 
        {
        }
    }
    
    // RECEPCION CLIENTE (O)
    void OnPacketReceive(Packet packet)
    {
        switch(packet.getOpcode())
        {
            case TICK:
                serverTickHandler(packet.getData());
                break;
            case UPD:
                serverUpdateHandler(packet.getData());
                break;
            case PING: // No hacemos nada, la simple recepcion es suficiente.
                break;
            default:
        }
    }

    // EVENTOS DE TRANSMISION: PREPARAN EL PAQUETE PARA SER ENVIADO Y REGISTRAN LOS EVENTOS.
    // Transmision Reset: opcode, counterid (O)
    public boolean resetCounter(int id) 
    {
        // Solo transmite si esta el cliente en estado listo
        if (m_clientState != clientState.BOUND_CONNECTED)
            return false;
        // Abortamos si el valor no es valido
        if (id == -1)
            return false;
        
        // Buscamos el counter de esta ID
        Counter counter = Shared.getCounterById(m_counters, id);
        // Abortamos si no existe
        if (counter == null)
            return false;
        
        // Registramos evento
        printLogEvent("RESET EVENT: Counter " + id + " has been reset to " + counter.getStartingTime() + "ms.");
        
        // Construimos paquete
        List<String> data = new ArrayList<String>();
        data.add(String.valueOf(id));
        Packet packet = new Packet(Packet.Opcode.RES, m_clientListenerPort, data);
        sendPacket(packet);
        
        // Reportamos exito
        return true;
    }

    // Transmision Modify: opcode, counterid, newvalue (O)
    public boolean modifyCounter(int id, int newValue) 
    {
        // Solo transmite si esta el cliente en estado listo
        if (m_clientState != clientState.BOUND_CONNECTED)
            return false;
        // Abortamos si el valor no es valido
        if (id == -1 || newValue == -1)
            return false;
        
        // Buscamos el counter de esta ID
        Counter counter = Shared.getCounterById(m_counters, id);
        // Abortamos si no existe
        if (counter == null)
            return false;
        
        // Registramos evento
        printLogEvent("MODIFY EVENT: Counter " + id + " value has been set to " + newValue + "ms.");
        
        // Construimos paquete
        List<String> data = new ArrayList<String>();
        data.add(String.valueOf(id));
        data.add(String.valueOf(newValue));
        Packet packet = new Packet(Packet.Opcode.MOD, m_clientListenerPort, data);
        sendPacket(packet);
        
        // Reportamos exito
        return true;
    }
    
    // Transmision Delete: opcode, counterid (O)
    public boolean deleteCounter(int id) 
    {
        // Solo transmite si esta el cliente en estado listo
        if (m_clientState != clientState.BOUND_CONNECTED)
            return false;
        // Abortamos si el valor no es valido
        if (id == -1)
            return false;
        
        // Buscamos el counter de esta ID
        Counter counter = Shared.getCounterById(m_counters, id);
        // Abortamos si no existe
        if (counter == null)
            return false;
        
        // Registramos evento
        printLogEvent("DELETE EVENT: Counter " + id + " has been deleted.");
        
        // Construimos paquete
        List<String> data = new ArrayList<String>();
        data.add(String.valueOf(id));
        Packet packet = new Packet(Packet.Opcode.DEL, m_clientListenerPort, data);
        sendPacket(packet);
        
        // Reportamos exito
        return true;
    }
    
    // Transmision Add: opcode, newvalue (O)
    public boolean addCounter(int value, String tickString) 
    {
        // Solo transmite si esta el cliente en estado listo
        if (m_clientState != clientState.BOUND_CONNECTED)
            return false;
        // Abortamos si el valor no es valido
        if (value == -1)
            return false;
        
        // Registramos evento
        printLogEvent("NEW COUNTER: New counter with value " + value + "ms and tick string '" + tickString + "' has started.");
        
        List<String> data = new ArrayList<String>();
        data.add(String.valueOf(value));
        data.add(tickString);
        Packet packet = new Packet(Packet.Opcode.NEW, m_clientListenerPort, data);
        sendPacket(packet);
        
        // Reportamos exito
        return true;
    }
    
    // Transmision Ping: opcode
    protected void sendPingToServer() 
    {
        // Si el cliente no ha bindeado puerto no transmitira
        if (m_clientState == clientState.UNBOUND_UNCONNECTED)
            return;

        List<String> data = new ArrayList<String>();
        data.add(""); // no datos
        Packet packet = new Packet(Packet.Opcode.PING, m_clientListenerPort, data);
        sendPacket(packet);
    }
    
    // EVENTOS DE RECEPCION: SE EJECUTAN ESTAS ACCIONES AL RECIBIR EL PAQUETE CORRESPONDIENTE
    // Funcion que se ejecuta cuando recibimos un tick (O)
    private void serverTickHandler(List<String> data)
    {
        // Leemos id del counter.
        int counterId = Shared.tryParse(data.get(0));
        String tickString = data.get(1);
        
        // Si no se lee correctamente, abortar.
        if (counterId == -1)
            return;
        
        // Buscamos el counter de esta ID
        Counter counter = Shared.getCounterById(m_counters, counterId);
        // Abortamos si no existe
        if (counter == null)
            return;
        
        // Registramos evento
        printLogEvent("TICK EVENT: Counter " + counterId + " with initial value " + counter.getStartingTime() +"ms has expired." + " STRING: " + tickString);
        
        // Print string en consola
        System.out.println(tickString);
        
        // TODO: Insertar aqui el codigo deseado para cuando haga tick determinado contador
    }
    
    // Funcion que se ejecuta cuando recibimos un update (O)
    private void serverUpdateHandler(List<String> data)
    {
        // Eliminamos todos los contadores
        clearAllCounters();
        
        // Leemos array de datos e instanciamos cada contador recibido
        for (int i = 0; i < data.size(); i = i + 6)
        {
            // Leemos los nuevos
            int counterId = Shared.tryParse(data.get(0 + i));
            int startingTime = Shared.tryParse(data.get(1 + i));
            int remainingTime = Shared.tryParse(data.get(2 + i));
            String tickString = (data.get(3 + i));
            String clientIp = data.get(4 + i);
            int clientPort = Shared.tryParse(data.get(5 + i));
            
            // Instanciamos cliente
            Client client = new Client(clientIp, clientPort);
            
            // Si no hay error en el parse
            if (counterId != -1 && startingTime != -1 && remainingTime != -1)
                createCounter(counterId, startingTime, remainingTime, client, tickString);
        }
        
        // Actualizamos UI
        updateCountersDisplay();
    }

    
    /// ############
    /// ###VARIOS###
    /// ############
    
    // Elimina todos los contadores (O)
    private void clearAllCounters()
    {
        m_counters.clear();
    }
    
    // Metodo para crear un nuevo counter y meterlo en la lista con los dem�s (O)
    private void createCounter(int id, int startValue, int currentValue, Client client, String tickString) 
    {
        m_counters.add(new Counter(id, startValue, currentValue, client, tickString));
    }
    
    // Reinicia el contador de timeout
    private void resetTimeoutTimer() 
    {
        m_connectionTimeOut = Shared.TIMER_CLIENT_PING_PERIOD * 2;
    }
    
    // Setea el estado del server (true = online, false = offline) (O)
    void setServerState(boolean online) 
    {
        // Si esta online, reiniciamos timer de connection timeout, pues acabamos de recibir una respuesta a un ping.
        if (online)
            resetTimeoutTimer();
        
        // Si el server no estaba online y pasa a estarlo
        if (m_clientState != clientState.BOUND_CONNECTED && online)
        {
            // Actualizamos estado
            updateClientState(clientState.BOUND_CONNECTED);
            
            // Mensaje en el log
            printLogEvent("Connection established with the server at: " + m_serverIp);
            if (!GraphicsEnvironment.isHeadless()) // Si tiene interfaz
                m_serverStateLabel.setIcon(new ImageIcon(ClientMain.class.getResource("/Shared/sOnline.jpg")));
            
            // Hack para consola, necesario pulsar una tecla para continuar
            System.out.println("Connection established with the server at: " + m_serverIp + ". Press any key to continue.");
            
            // Activamos envio de pings permanentemente
            try
            {
                m_pingTimer.scheduleAtFixedRate(m_sendPing, 0, Shared.TIMER_CLIENT_PING_PERIOD);
            }
            catch (Exception e)
            {
            }
        }
        else if (m_clientState == clientState.BOUND_CONNECTED && !online) // Si el server estaba online y pasa a dejar de estarlo
        {
            // Actualizamos estado
            updateClientState(clientState.BOUND_UNCONNECTED);
            
            // Mensaje en el log
            printLogEvent("Connection lost with the server at: " + m_serverIp);
            if (!GraphicsEnvironment.isHeadless()) // Si tiene interfaz
                m_serverStateLabel.setIcon(new ImageIcon(ClientMain.class.getResource("/Shared/sOffline.jpg")));
            
            // Hack para consola, necesario pulsar una tecla para continuar
            System.out.println("Connection lost with the server at: " + m_serverIp + ". Press any key to continue.");
            
            // Borramos todos los contadores y actualizamos la interfaz, ya que han dejado de existir al caerse el servidor, si se conecta volvera a recibirlos e imprimirlos
            clearAllCounters();
            updateCountersDisplay();
        }

    }
    
    // Actualizacion de estado del cliente y enables de interfaz
    public void updateClientState(clientState newState)
    {
        // Establecemos estado
        m_clientState = newState;
        
        // Abortamos si no hay interfaz
        if (GraphicsEnvironment.isHeadless())
            return;
        
        // Actuamos en consecuencia
        switch (m_clientState)
        {
            case UNBOUND_UNCONNECTED:
                // Interfaz bind
                m_clientListenPortTextBox.setEnabled(true);
                m_bindPortButton.setEnabled(true);
                m_portStateLabel.setEnabled(true);
                
                // Interfaz connect
                m_serverIpTextBox.setEnabled(false);
                m_serverConnectButton.setEnabled(false);
                m_serverStateLabel.setEnabled(false);
                
                // UI restante
                m_addCounterButton.setEnabled(false);
                m_tickStringTextBox.setEnabled(false);
                m_activeCounters.setEnabled(false);
                break;
            case BOUND_UNCONNECTED:
                // Interfaz bind
                m_clientListenPortTextBox.setEnabled(false);
                m_bindPortButton.setEnabled(false);
                m_portStateLabel.setEnabled(true); // Estado activado
                
                // Interfaz connect
                m_serverIpTextBox.setEnabled(true);
                m_serverConnectButton.setEnabled(true);
                m_serverStateLabel.setEnabled(true);
                
                // UI restante
                m_addCounterButton.setEnabled(false);
                m_tickStringTextBox.setEnabled(false);
                m_activeCounters.setEnabled(false);
                break;
            case BOUND_CONNECTED:
                m_clientListenPortTextBox.setEnabled(false);
                m_bindPortButton.setEnabled(false);
                m_portStateLabel.setEnabled(true); // Estado activado
                
                // Interfaz connect
                m_serverIpTextBox.setEnabled(false);
                m_serverConnectButton.setEnabled(false);
                m_serverStateLabel.setEnabled(true); // Estado activado
                
                // UI restante
                m_addCounterButton.setEnabled(true);
                m_tickStringTextBox.setEnabled(true);
                m_activeCounters.setEnabled(true);
                break;
        }
    }

    // Devuelve todos los contadores
    public List<Counter> getAllCounters() 
    {
        return m_counters;
    }
}
