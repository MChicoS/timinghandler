package Client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import Shared.Packet;
import Shared.Shared;


public class ClientListenThread extends Thread
{
    private ClientMain m_mainWindow;
    
    // Ventana que ha ejecutado el thread
    public ClientListenThread(ClientMain window) 
    {
        m_mainWindow = window;
    }

    public void run()
    {
        while (true)
            listenServerRequests();
    }
    
    // RECEPCION CLIENTE: Escucha al servidor y espera su accept para recibir datos.
    private void listenServerRequests()
    {
        try
        {
            // Esperamos accept
            Shared.clientSocket = Shared.serverSocket.accept();
            
            // Se obtiene el flujo entrante desde el servidor
            BufferedReader entrada = new BufferedReader(new InputStreamReader(Shared.clientSocket.getInputStream()));
            
            // Mensaje recibido
            String message = entrada.readLine();
            // Construimos packet
            Packet packet = Shared.buildPacketFromMessage(message);
            
            // Ejecutamos metodo de recepcion que asigna metodo a cada opcode.
            m_mainWindow.OnPacketReceive(packet);
            
            // Se recibe el paquete correctamente
            m_mainWindow.setServerState(true);
        }
        catch (Exception e)
        {
        }
    }
}