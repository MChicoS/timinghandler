package Server;

import java.util.List;
import java.util.Scanner;
import Shared.Client;
import Shared.Counter;
import Shared.Shared;

public class ServerConsoleInputListener extends Thread
{
    public enum Commands 
    {
        help,     // Muestra sus comandos y sintaxis:                         help
        show,     // Muestra todos los contadores del cliente en pantalla:    show
        res,    // Reinicia el contador con la ID dada:                        res <ID> <CLIENT IP> <CLIENT PORT>
        resall,    // Reinicia todos los contadores del server:                 resall
        mod,     // Modifica el contador con la ID dada:                        mod <ID> <CLIENT IP> <CLIENT PORT> <VALUE>
        del,     // Borra el contador con la ID dada                            del <ID> <CLIENT IP> <CLIENT PORT>
        ping,   // Envia un ping al cliente                                      ping <CLIENT IP> <CLIENT PORT>
    }

    private ServerMain m_mainWindow;
    Scanner m_scanIn;
    
    // Ventana que ha ejecutado el thread
    public ServerConsoleInputListener(ServerMain window) 
    {
        m_mainWindow = window;
    }

    public void run()
    {
        m_scanIn = new Scanner(System.in);
        System.out.println("Timing Handler v1.2 (SERVER)");
        System.out.println("List of available comamands:");
        for (Commands com : Commands.values()) 
            System.out.println(getCommandHelp(com));
        // Lectura permanente de comandos llegados a este punto
        while (true)
            readConsoleCommands();
    }

    // Modo lectura de comandos si el cliente ya esta online
    private void readConsoleCommands()
    {
        System.out.println("Enter command: ");
        String commandString = m_scanIn.nextLine();
        parseCommand(commandString);
    }

    private void parseCommand(String commandString) 
    {
        String[] commandArray = commandString.split(" ");
        
        // Intentamos parsear comando
        try
        {
            Commands command = Commands.valueOf(commandArray[0]);
            switch (command)
            {
                case help: // No params
                    System.out.println("List of available comamands:");
                    for (Commands com : Commands.values()) 
                        System.out.println(getCommandHelp(com));
                    break;
                case show: // No params
                    System.out.println("Showing all counters:");
                    // Leemos los contadores y printeamos
                    List<Counter> counters = m_mainWindow.getAllCounters();
                    for (int i = 0; i < counters.size(); i++)
                        System.out.println("Counter " + counters.get(i).getId() + " from "  + counters.get(i).getClient().getIpAdress() + "(" + counters.get(i).getClient().getListenPort() + ")" + " value " + counters.get(i).getStartingTime() + "ms/" + counters.get(i).getRemainingTime() + "ms and tick string: " + counters.get(i).getTickString());
                    break;
                case res: // id, clientIp, clientPort
                    if (commandArray.length != 4)
                    {
                        System.out.println("Error parsing command '" + command + "' sintax: "  + getCommandHelp(command));
                        break;
                    }    
                    if (m_mainWindow.resetCounter(Shared.tryParse(commandArray[1]), new Client(commandArray[2], Shared.tryParse(commandArray[3])), true))
                        System.out.println(("RESET: Counter " + commandArray[1] + " from " + commandArray[2] + "(" + commandArray[3] + ")" + " has been reset to " + Shared.getCounterByIdAndClient(m_mainWindow.getAllCounters(), Shared.tryParse(commandArray[1]), new Client(commandArray[2], Shared.tryParse(commandArray[3]))).getStartingTime() + "ms."));
                    else
                        System.out.println("Error parsing command '" + command + "' sintax: "  + getCommandHelp(command));
                    break;
                case resall: // No params
                    m_mainWindow.resetAllCounters();
                    System.out.println("All counters has been reset.");
                    break;
                case mod: // id, clientip, clientport, value
                    if (commandArray.length != 5)
                    {
                        System.out.println("Error parsing command '" + command + "' sintax: "  + getCommandHelp(command));
                        break;
                    }    
                    if (m_mainWindow.modifyCounter(Shared.tryParse(commandArray[1]), new Client(commandArray[2], Shared.tryParse(commandArray[3])) , Shared.tryParse(commandArray[4])))
                        System.out.println("MODIFY: Counter " + commandArray[1] + " from " + commandArray[2] + "(" + commandArray[3] + ")" +  " value has been set to " + commandArray[4] + "ms.");
                    else
                        System.out.println("Error parsing command '" + command + "' sintax: "  + getCommandHelp(command));
                    break;
                case del: // id, clientip, clientport
                    if (commandArray.length != 4)
                    {
                        System.out.println("Error parsing command '" + command + "' sintax: "  + getCommandHelp(command));
                        break;
                    }
                    if (m_mainWindow.deleteCounter(Shared.tryParse(commandArray[1]),  new Client(commandArray[2], Shared.tryParse(commandArray[3]))))
                        System.out.println("DELETE: Counter " + commandArray[1] + " from " + commandArray[2] + "(" + commandArray[3] + ")" +  " has been deleted.");
                    else
                        System.out.println("Error parsing command '" + command + "' sintax: "  + getCommandHelp(command));
                    break;
                case ping: // clientip, clientport
                    if (commandArray.length != 3)
                    {
                        System.out.println("Error parsing command '" + command + "' sintax: "  + getCommandHelp(command));
                        break;
                    }
                    m_mainWindow.sendPingBackToClient(new Client(commandArray[1], Shared.tryParse(commandArray[2])));
                    break;
            }
        }
        catch (Exception e)
        {
            System.out.println("Unknown command. Tipe 'help' to view avaliable commands.");
        }
    }

    private String getCommandHelp(Commands command)
    {
        String output = "";
        switch (command)
        {
            case help:
                output = "'help' shows all avaliable commands.";
                break;
            case show:
                output = "'show' print all counters.";
                break;
            case res:
                output = "'res <id> <client ip> <client port>' Resets the counter with the defined id for the defined client.";
                break;
            case resall:
                output = "'resall' Resets all counters of the server.";
                break;
            case mod:
                output = "'mod <id> <client ip> <client port> <value>' Sets the value of the counter with the defined id for the defined client to the defined value.";
                break;
            case del:
                output = "'del <id> <client ip> <client port>' Deletes the counter with the defined id for the defined client.";
                break;
            case ping:
                output = "'ping <client ip> <client port>' Sends a ping to the defined client.";
                break;
            default:
                output = "Unknown command. Tip 'help' to view avaliable commands.";
        }
        return output;
    }
}
