package Server;

import java.awt.EventQueue;
import javax.swing.JFrame;
import net.miginfocom.swing.MigLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.DefaultListModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import Shared.Client;
import Shared.Counter;
import Shared.Packet;
import Shared.Shared;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;

public class ServerMain
{
    /*
    // Sirve para testear el modo headless (sin interfaz) 
    static 
    { 
        System.setProperty("java.awt.headless", "true");
    }
    */
    
    // UI
    private JPanel m_activeCounters;
    JList<String> m_eventLog;
    DefaultListModel<String> m_eventLogList;
    JScrollPane m_countersScrollPanel;
    JScrollPane m_eventLogScroll;

    // Variables
    private List<Counter> m_counters = new ArrayList<Counter>();

    // Metodo main, iniciamos el server (O)
    public static void main(String[] args) 
    {
        EventQueue.invokeLater(new Runnable() 
        {
            public void run() 
            {
                try 
                {
                    // Al lanzar el thread asignamos los sockets, si no pudiera asignarlos saltaria una excepcion y no ejecutariamos la ventana.
                    Shared.serverSocket = new ServerSocket(Shared.CLIENT_TO_SERVER_PORT);
                    Shared.clientSocket = new Socket();
                } 
                catch (Exception e) 
                {
                    System.out.println("Another server instance is already running.");
                    return;
                }
                
                // Inicializamos la ventana
                ServerMain window = new ServerMain();
                
                // Inicializamos thread que escucha al servidor
                ServerListenThread listenThread = new ServerListenThread(window);
                listenThread.start();
                
                // Inicializamos thread de consola
                ServerConsoleInputListener consoleListenThread = new ServerConsoleInputListener(window);
                consoleListenThread.start();
            }
        });
    }
    
    // Constructor, inicializamos toda la UI (O)
    public ServerMain() 
    {
        // Inicializamos interfaz si existe
        if (!GraphicsEnvironment.isHeadless())
            initializeUi();
        
        // Temporizador general
        Timer timer = new Timer();
        // Decrementacion del valor de los contadores
        TimerTask clockTick = new TimerTask()
        {
            public void run() 
            {
                serverTimerTick(Shared.TIMER_SERVER_PERIOD);
            }
        };
        // Actualizacion de la UI del servidor con el valor de los contadores
        TimerTask uiUpdateTick = new TimerTask()
        {
            public void run() 
            {
                updateCountersDisplay();
            }
        };
        // Actualizacion forzada a los clientes de los contadores
        TimerTask clientUpdateTick = new TimerTask()
        {
            public void run() 
            {
                updateAllCounterClients();
            }
        };
        timer.scheduleAtFixedRate(clockTick, 0, Shared.TIMER_SERVER_PERIOD);
        timer.scheduleAtFixedRate(uiUpdateTick, 0, Shared.TIMER_SERVER_UPDATE_UI_PERIOD);
        timer.scheduleAtFixedRate(clientUpdateTick, 0, Shared.TIMER_CLIENTS_UPDATE_PERIOD);
    }
    
    
    /// ##############
    /// ###INTERFAZ###
    /// ##############

    // Metodo que inicializa todas las variables de la interfaz (O)
    public void initializeUi()
    {
        // Jframe principal
        JFrame mainWindowJFrame = new JFrame("Timing Handler v1.2 (SERVER)");
        mainWindowJFrame.setResizable(false);
        mainWindowJFrame.setBounds(100, 100, 731, 384);
        mainWindowJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainWindowJFrame.getContentPane().setLayout(new MigLayout("", "[237px][10px][69px][8px][63px][8px][69px][4px][233px]", "[14px][314px][23px][]"));
        mainWindowJFrame.setVisible(true);
        
        // label de active counters
        JLabel activeCountersLabel = new JLabel("Active counters");
        activeCountersLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
        mainWindowJFrame.getContentPane().add(activeCountersLabel, "cell 0 0,alignx left,aligny top");
        
        // Label de log de eventos
        JLabel logEventsLabel = new JLabel("Packets Log");
        logEventsLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
        mainWindowJFrame.getContentPane().add(logEventsLabel, "cell 2 0 5 1,alignx left,aligny top");
        
        // Panel de scroll de los contadores
        m_countersScrollPanel = new JScrollPane();
        m_countersScrollPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        mainWindowJFrame.getContentPane().add(m_countersScrollPanel, "cell 0 1 2 2,grow");
        
        // Panel de contadores
        m_activeCounters = new JPanel();
        m_activeCounters.setLayout(null);
        m_activeCounters.setPreferredSize(new Dimension(0, 0));

        // Log de eventos, scroll y tabla
        m_eventLogList = new DefaultListModel<String>();
        m_eventLogScroll = new JScrollPane();
        mainWindowJFrame.getContentPane().add(m_eventLogScroll, "cell 2 1 7 2,grow");
        m_eventLog = new JList<String>(m_eventLogList);
        m_eventLogScroll.setViewportView(m_eventLog);
        m_eventLog.setMinimumSize(new Dimension(50, 50));
        m_eventLog.setFont(new Font("Tahoma", Font.PLAIN, 11));
        
        // Boton de reset all
        JButton resetAllCountersButton = new JButton("Reset All Counters");
        mainWindowJFrame.getContentPane().add(resetAllCountersButton, "cell 0 3,growx,aligny center");
        resetAllCountersButton.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
                resetAllCounters();
            }
        });


        // Boton Clear Log
        JButton clearLogButton = new JButton("Clear Log");
        mainWindowJFrame.getContentPane().add(clearLogButton, "flowx,cell 2 3,alignx left");
        clearLogButton.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e)
            {
                clearLog();
            }
        });
    }

    // Imprimimos nuevo evento en el log de eventos con la fecha de este instante (O)
    public void printLogEvent(final String text)
    {
        // Instante actual, en milisegundos.
        Format format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:SSS");
        final String dateString = format.format(Calendar.getInstance().getTime());

        // Print en consola
        System.out.println(">>" + dateString + " " + text);
        
        // Si no hay interfaz grafica, abortar
        if (GraphicsEnvironment.isHeadless())
            return;
            
        // Aseguramos que el update se encuentre en un thread independiente
        SwingUtilities.invokeLater(new Runnable() 
        {
            public void run() 
            {
                // add elemento.
                m_eventLogList.addElement(dateString + " " + text);
                
                // Longitud horizontal maxima para actualizar scroll
                int longestLength = Shared.getLongestStringOfLogList(m_eventLogList); 
                float pixelsPerChar = 5.5f; // Pixeles por caracter, usado para calcular la longitud visual de los strings.
                int finalSize = (int)(longestLength * pixelsPerChar);

                // Establecemos nueva longitud del panel que contiene los contadores.
                m_eventLog.setPreferredSize(new Dimension(finalSize, m_eventLogList.size() * 16));
            
                // Establecemos scroll del panel scrollable que lo contiene
                m_eventLogScroll.setViewportView(m_eventLog);
            }
        });
    }
    
    // Limpiamos todo el log (O)
    public void clearLog()
    {
        // Si no hay interfaz grafica, abortar
        if (GraphicsEnvironment.isHeadless())
            return;
        
        // Limpiamos log
        m_eventLogList.clear();
        
        // Restablecemos scroll
        m_eventLog.setPreferredSize(new Dimension(0, 0));
        m_eventLogScroll.setViewportView(m_eventLog);
    }
    
    // Actualiza el display de counters con sus valores actuales. (O)
    private void updateCountersDisplay()
    {
        // Si no hay interfaz grafica, abortar
        if (GraphicsEnvironment.isHeadless())
            return;
        
        // Aseguramos que el update se encuentre en un thread independiente
        SwingUtilities.invokeLater(new Runnable() 
        {
            public void run() 
            {
                // Limpiamos display
                m_activeCounters.removeAll();
                m_activeCounters.setPreferredSize(new Dimension(0, 0));
                
                // Cargamos todos los contadores
                for (int i = 0; i < m_counters.size(); i++)
                    addNewTimerPanel(m_counters.get(i).getId(), m_counters.get(i).getStartingTime(), m_counters.get(i).getRemainingTime(), m_counters.get(i).getClient(), i);
                
                // Repintamos la UI
                m_activeCounters.repaint();
            }
        });

    }
    
    // Anade un nuevo contador y sus controles asociados (O)
    public void addNewTimerPanel(final int counterId, int startingMs, int remainingMs, final Client client, int rowNumber)
    {
        // Si no hay interfaz grafica, abortar
        if (GraphicsEnvironment.isHeadless())
            return;

        // Las posiciones son incrementales
        int heightGain = 60; // Separacion con respecto a la fila previa
        int pixelsPerChar = 6; // Pixeles por caracter, usado para calcular la longitud visual de los strings.
        int offSetCorrection = 4; // Correccion de posicion para valores altos.
        
        // Label: nombre del contador.
        JLabel counterNameLabel = new JLabel("Counter " + counterId + " From " + client.getIpAdress() + "(" + client.getListenPort() + ")");
        counterNameLabel.setBounds(0, heightGain * rowNumber, 200, 20);
        m_activeCounters.add(counterNameLabel);
        // Label: tiempo inicial del contador
        JLabel counterStartingTimeLabel = new JLabel(startingMs + "ms / ");
        counterStartingTimeLabel.setBounds(0, (heightGain * rowNumber) + 15, 120, 20);
        m_activeCounters.add(counterStartingTimeLabel);
        // Label: tiempo restante del contador.
        JLabel counterRemainingTimeLabel = new JLabel(remainingMs + "ms");
        counterRemainingTimeLabel.setBounds(counterStartingTimeLabel.getText().length() * pixelsPerChar + offSetCorrection, (heightGain * rowNumber) + 15, 120, 20);
        m_activeCounters.add(counterRemainingTimeLabel);
        
        // Botones
        JButton resetButton = new JButton("Reset");
        resetButton.setBounds(0, (heightGain * rowNumber) + 35, 70, 20);
        m_activeCounters.add(resetButton);
        JButton modifyButton = new JButton("Modify");
        modifyButton.setBounds(70, (heightGain * rowNumber) + 35, 80, 20);
        m_activeCounters.add(modifyButton);
        JButton deleteButton = new JButton("Delete");
        deleteButton.setBounds(150, (heightGain * rowNumber) + 35, 70, 20);
        m_activeCounters.add(deleteButton);

        // Acciones de botones
        resetButton.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
                resetCounter(counterId, client, true);
            }
        });
        modifyButton.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
                String result = JOptionPane.showInputDialog("Enter the new value in ms:");

                // Si no pulsamos cancelar
                if (result != null)
                {
                    int value = Shared.tryParse(result);
                    if (!modifyCounter(counterId, client, value))
                        JOptionPane.showMessageDialog(null, "Insert a valid number.");
                }
            }
        });
        deleteButton.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
                deleteCounter(counterId, client);
            }
        });

        // Establecemos nueva dimension del scroll
        // dimension actual
        Dimension dim = m_activeCounters.getPreferredSize();
        // Establecemos nueva longitud del panel que contiene los contadores.
        m_activeCounters.setPreferredSize(new Dimension(220, dim.height + heightGain));
        // Establecemos scroll del panel scrollable que lo contiene
        m_countersScrollPanel.setViewportView(m_activeCounters);
    }
    
    // Boton de reiniciar todos los contadores pulsado (O)
    void resetAllCounters()
    {
        // HACK: No reiniciamos cada contador independiente para prevenir spam de paquetes de reset, sino que tras haber reiniciamos mandamos un update a todos los clientes
        
        // Barremos todos los contadores y los reiniciamos
        for (int i = 0; i < m_counters.size(); i++)
            resetCounter(m_counters.get(i).getId(), m_counters.get(i).getClient(), false);
        
        // Mandamos un update a todos los clientes
        updateAllCounterClients();
    }
    
    /// ####################
    /// ###COMUNICACIONES###
    /// ####################
    
    // TRANSMISION SERVIDOR (O)
    private void sendPacket(final Packet packet, final Client client, final boolean showInLog)
    {
        // Construimos el mensaje
        String message = Shared.buildMessageFromPacket(packet);

        // Enviamos paquete
        try 
        {
            // Creamos socket
            Shared.clientSocket = new Socket(client.getIpAdress(), client.getListenPort());
            
            // Salida de datos
            Shared.outputData = new DataOutputStream(Shared.clientSocket.getOutputStream());
            Shared.outputData.writeUTF(message + "\n");                
            
            //Fin de la conexion
            Shared.clientSocket.close();
            
            // Log
            if (showInLog)
                printLogEvent("Sent packet OPCODE: " + packet.getOpcode() + "; DATA: " + Shared.stringJoin(",", packet.getData()) + "; TO: " + client.getIpAdress() + "(" + client.getListenPort() +")");
        } 
        catch (IOException e) 
        {
            if (showInLog)
                printLogEvent("Error sending packet: client " + client.getListenPort() + "(" + client.getListenPort() +")" + " offline.");
        }
    }
    
    
    // RECEPCION SERVIDOR (O)
    void OnPacketReceive(Packet packet, Client client, boolean showInLog)
    {
        // Log
        if (showInLog)
            printLogEvent("Received packet OPCODE: " + packet.getOpcode() + "; DATA: " + Shared.stringJoin(",", packet.getData()) + "; FROM: " + client.getIpAdress() + "(" + client.getListenPort() + ")");
        
        switch(packet.getOpcode())
        {
            case RES: // Reset: counterId
                if (!resetCounter(Shared.tryParse(packet.getData().get(0)), client, true))
                    printLogEvent("Error executing handler for opcode: " + packet.getOpcode() + " due to invalid data.");
                break;
            case DEL: // Delete: counterId
                if (!deleteCounter(Shared.tryParse(packet.getData().get(0)), client))
                    printLogEvent("Error executing handler for opcode: " + packet.getOpcode() + " due to invalid data.");
                break;
            case MOD: // Modify: counterId, value
                if (!modifyCounter(Shared.tryParse(packet.getData().get(0)), client, Shared.tryParse(packet.getData().get(1))))
                    printLogEvent("Error executing handler for opcode: " + packet.getOpcode() + " due to invalid data.");
                break;
            case NEW: // New: value
                if (!addCounter(Shared.tryParse(packet.getData().get(0)), client, packet.getData().get(1)))
                    printLogEvent("Error executing handler for opcode: " + packet.getOpcode() + " due to invalid data.");
                break;
            case PING: // Ping
                sendPingBackToClient(client);
                break;
            default:
                System.out.println("Unknown opcode received.");
                break;
        }
    }

    // EVENTOS DE TRANSMISION: PREPARAN EL PAQUETE PARA SER ENVIADO.
    // Transmision Tick: opcode, counterid (O)
    private void tickCounter(int id, Client client, String tickString) 
    {
        List<String> data = new ArrayList<String>();
        data.add(String.valueOf(id));
        data.add(tickString);
        Packet packet = new Packet(Packet.Opcode.TICK, client.getListenPort(), data);
        sendPacket(packet, client, true);
    }
    
    // Transmision Update: opcode, datoscounter1, datoscounter2, datoscounter3.... (O)
    private void updateClient(Client client, boolean showInLog) 
    {
        // Leemos contadores de este cliente
        List<Counter> countersFromClient = Shared.getCountersByClient(m_counters, client);
        
        // Nuevo array de datos que rellenaremos con los datos de los contadores
        List<String> data = new ArrayList<String>();
        for (int i = 0; i < countersFromClient.size(); i++)
        {
            Counter counter = countersFromClient.get(i);
            data.add(String.valueOf(counter.getId()));
            data.add(String.valueOf(counter.getStartingTime()));
            data.add(String.valueOf(counter.getRemainingTime()));
            data.add(String.valueOf(counter.getTickString()));
            data.add(String.valueOf(counter.getClient().getIpAdress()));
            data.add(String.valueOf(counter.getClient().getListenPort()));
        }
        
        // Construimos paquete y ejecutamos envio
        Packet packet = new Packet(Packet.Opcode.UPD, client.getListenPort(), data);
        sendPacket(packet, client, showInLog);
    }

    // Transmision Ping (check online state) (O)
    void sendPingBackToClient(Client client) 
    {
        List<String> data = new ArrayList<String>();
        data.add(""); // No datos
        Packet packet = new Packet(Packet.Opcode.PING, client.getListenPort(), data);
        sendPacket(packet, client, false);
    }
    
    // EVENTOS DE RECEPCION: SE EJECUTAN ESTAS ACCIONES AL RECIBIR EL PAQUETE CORRESPONDIENTE (O AL PULSARLAS EN LA INTERFAZ)
    // Reset: Reiniciamos el contador con la id especificada del cliente que lo solicite. (O)
    boolean resetCounter(int counterId, Client client, boolean updateClient) 
    {
        // Abortamos si el valor no es valido
        if (counterId == -1 || client.getListenPort() == -1)
            return false;

        // Buscamos counter
        Counter counter = Shared.getCounterByIdAndClient(m_counters, counterId, client);
        // Abortamos si no existe
        if (counter == null)
            return false;
        
        // Reiniciamos contador a su valor original
        counter.setRemainingTime(counter.getStartingTime());
        
        // Update UI
        updateCountersDisplay();
        
        // Enviamos un update al cliente
        if (updateClient)
            updateClient(client, true);
        
        // Reportamos exito
        return true;
    }
    
    // Reset: Modificamos el tiempo restante del contador con la id especificada del cliente que lo solicite en la cantidad especificada. (O)
    boolean modifyCounter(int counterId, Client client, int value)
    {
        // Abortamos si el valor no es valido
        if (counterId == -1 || value == -1 || client.getListenPort() == -1)
            return false;

        // Buscamos counter
        Counter counter = Shared.getCounterByIdAndClient(m_counters, counterId, client);
        // Abortamos si no existe
        if (counter == null)
            return false;
        
        // Establecemos nuevo valor para el contador
        counter.setRemainingTime(value);
        counter.setStartingTime(value);

        // Update UI
        updateCountersDisplay();
        
        // Enviamos un update al cliente
        updateClient(client, true);
        
        // Reportamos exito
        return true;
    }
    
    // Delete: Eliminamos el contador con la id especificada del cliente que lo solicite. (O)
    boolean deleteCounter(int counterId, Client client)
    {
        // Abortamos si el valor no es valido
        if (counterId == -1 || client.getListenPort() == -1)
            return false;

        // Buscamos counter
        Counter counter = Shared.getCounterByIdAndClient(m_counters, counterId, client);
        // Abortamos si no existe
        if (counter == null)
            return false;
        
        // Eliminamos el contador
        removeCounter(counter);
        
        // Update UI
        updateCountersDisplay();
        
        // Enviamos un update al cliente
        updateClient(client, true);
        
        // Reportamos exito
        return true;
    }
    
    // Add: Add nuevo contador con el valor especificado para el cliente solicitante y le asignamos la id correspondiente. (O)
    private boolean addCounter(int value, Client client, String tickString) 
    {
        // Abortamos si el valor no es valido
        if (value == -1 || client.getListenPort() == -1)
            return false;

        // Generamos la ID correcta para asignar
        int newId = getNewIdToAsignToCounter(client);
        
        // Creamos el contador
        createCounter(newId, value, value, client, tickString);
        
        // Forzamos una actualizacion de la interfaz para que se muestre
        updateCountersDisplay();
        
        // Enviamos un update al cliente
        updateClient(client, true);
        
        // Reportamos exito
        return true;
    }

    
    /// ############
    /// ###VARIOS###
    /// ############
    
    // Devuelve la ID que ha de asignarle a un nuevo contador de un cliente que ya tiene asignados contadores previos. (O)
    private int getNewIdToAsignToCounter(Client client) 
    {
        // No hay contadores, ID 0
        if (m_counters.size() == 0)
            return 0;
        
        // Obtenemos todos los contadores para este cliente
        List<Counter> countersByClient = Shared.getCountersByClient(m_counters, client);
        
        // Obtenemos Id mas alta de los contadores asignados a este cliente
        int higgerId = Shared.getHiggerCounterId(countersByClient);
        
        // La id a asignar es la siguiente
        return higgerId + 1;
    }

    // Metodo para crear un nuevo counter y meterlo en la lista con los demas.  (O)
    private void createCounter(int id, int startValue, int currentValue, Client client, String tickString) 
    {
        m_counters.add(new Counter(id, startValue, currentValue, client, tickString));
    }
    
    // Metodo para destruir un counter (O)
    private void removeCounter(Counter counter) 
    {
        m_counters.remove(counter);
    }
    
    // Se ejecuta cada periodo de timer (O)
    private void serverTimerTick(int period) 
    {
        // Barremos todos los contadores y actualizamos sus valores
        for (int i = 0; i < m_counters.size(); i++)
        {
            Counter counter = m_counters.get(i);
            
            // Decrementamos el contador en 1 periodo (tiempo entre ejecucion de esta funcion)
            int remainingTime = counter.getRemainingTime();
            counter.setRemainingTime(remainingTime - period);
            
            // Comprobamos si ha vencido el contador
            if (counter.getRemainingTime() <= 0)
            {
                // Ejecutamos tick
                tickCounter(counter.getId(), counter.getClient(), counter.getTickString());
                
                // Eliminamos contador
                deleteCounter(counter.getId(), counter.getClient());
            }
        }
    }
    
    // Se ejecuta cada periodo de update, envia a cada cliente informacion del estado de sus contadores. (O)
    private void updateAllCounterClients() 
    {
        // Obtenemos lista de todos los clientes
        List<Client> clients = Shared.getAllServerClients(m_counters);
        
        // Forzamos un update en todos ellos
        for (int i = 0; i < clients.size(); i++)
            updateClient(clients.get(i), false);
    }

    // Devuelve todos los contadores instanciados
    public List<Counter> getAllCounters() 
    {
        return m_counters;
    }
}
