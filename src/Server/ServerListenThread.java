package Server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import Shared.Client;
import Shared.Packet;
import Shared.Shared;
import Server.ServerMain;

public class ServerListenThread extends Thread
{
    private ServerMain m_mainWindow;
    
    // Ventana que ha ejecutado el thread
    public ServerListenThread(ServerMain window) 
    {
        m_mainWindow = window;
    }

    public void run()
    {
        // Listen infinito
        while (true)
            listenClientRequests();
    }
    
    // RECEPCION SERVER: Escucha a los clientes y espera su accept para recibir datos.
    private void listenClientRequests()
    {
        try
        {
            // Esperamos accept
            Shared.clientSocket = Shared.serverSocket.accept();
            
            // Se obtiene el flujo entrante desde el servidor
            BufferedReader entrada = new BufferedReader(new InputStreamReader(Shared.clientSocket.getInputStream()));

            // Mensaje recibido
            String message = entrada.readLine();

            // Construimos packet
            Packet packet = Shared.buildPacketFromMessage(message);

            // Paquetes que se reportan en el log
            boolean showInLog = true;
            switch(packet.getOpcode())
            {
                case PING:
                    showInLog = false;
                    break;
                case RES:
                case MOD:
                case DEL:
                case UPD:
                case NEW:
                case TICK: 
                default:
                    showInLog = true;
                    break;
            }

            // Obtenemos IP del cliente para su identificacion
            String ipAdress = Shared.clientSocket.getInetAddress().toString().replace("/", "");

            // Obtenemos puerto 
            int port = packet.getPort();

            // Nuevo cliente
            Client client = new Client(ipAdress, port);
            
            // Ejecutamos metodo de recepcion que asigna metodo a cada opcode.
            m_mainWindow.OnPacketReceive(packet, client, showInLog);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}
