Aplicación desarrollada en Java con fines de investigación: el objetivo es medir retardos en la recepción de señales propagadas a traves de una red local y el impacto que distintos elementos de la misma tienen sobre ello.

La aplicación trabaja en un diseño cliente/servidor, encontrándose ambos implementados en el proyecto. Se realiza también una completa gestión multithread para la gestión de contadores.

Permite su ejecución tanto en entorno gráfico como en entornos que no cuentan con el mismo, como interfaces Linux.